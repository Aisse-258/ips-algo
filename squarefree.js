'use strict';

var primes = require('./primes.json');
var primeSquares = primes.map(a => a**2);
var maxPrime = primes[primes.length-1];

function squarefree(n){
    if(n===0){
        return [0,0];
    }
    n = Math.abs(n);
    var multiplier = 1;
    var residual = 1;

    for(var h = 0; primeSquares[h] <= n && h < primes.length; h++){
        var i = primes[h];
        var j = primeSquares[h];
        while(n%j===0){
            n/=j;
            multiplier*=i;
        }
        if(n%i===0){
            n/=i;
            residual*=i;
        }
    }
    for(var i = maxPrime; i*i <= n ; i+=2){
        var j = i*i;
        while(n%j===0){
            n/=j;
            multiplier*=i;
        }
        if(n%i===0){
            n/=i;
            residual*=i;
        }
    }
    return [multiplier, residual*n];
}

module.exports = squarefree;