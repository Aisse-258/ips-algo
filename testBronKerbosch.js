'use strict';

const PointSet = require('./PointSet');

let ps = new PointSet(
    1,
    1,
    [
        {x:0, y:0},
        {x:0, y:3},
        {x:0, y:-3},
        {x:4, y:0},
        {x:4, y:3},
    ]
);


let ps2 = ps.deepClone();

ps.shift(0,1);

console.log(ps2);

//ps.applyBronKerbosch(0,4,o=>console.log(JSON.stringify(o)));