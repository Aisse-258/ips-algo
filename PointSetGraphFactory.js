const fs = require('fs');
const squarefree = require('./squarefree');
const PointSetGraph = require('./PointSetGraph');
const PointSetDraft = require('./PointSetDraft');
const PointSet = require('./PointSet');
const removeFromArray = require('./removeFromArray');

module.exports = class PointSetGraphFactory {

    constructor(){
        this.graphs={};
        this.drafts=[];
        this.ready=[];

        this.dontAppendCopiesInAdvance = false;
    }

    makePointSetGraphs(base, maxR){
        this.base = base * 2;
        var m = base;
        this.commonXpoints = [];
        for(var i = m*m-2*m; i > 0; i-=2*m){
            this.commonXpoints.push({x:i, y:0});
        }
        for(var a = 1; a <= maxR; a++){
            //console.log(a);
            for(var b = Math.max(a,m-a)+1; b <= maxR; b++){
                var x = b**2-a**2;
                var yRoot = -(m**4)+(2*a**2+2*b**2)*m**2-b**4+2*a**2*b**2-a**4;
                var y = squarefree(yRoot);
                var char = y[1], mult = y[0];
                this.ensureGraph(char);
                this.graphs[char].quad.push({x:x, y:mult});
            }
        }
        for(var a = Math.ceil((maxR+1)/2); a <= maxR; a++){
            var yRoot = 4*a**2*m**2-m**4;
            var y = squarefree(yRoot);
            var char = y[1], mult = y[0];
            this.ensureGraph(char);
            this.graphs[char].duaY.push({x:0, y:mult});            
        }
    }
    appendCopies(char){
        this.graphs[char].duaX = this.commonXpoints.slice();
        if(this.base%2 == 0){
            this.graphs[char].zero.push({x:0,y:0});
        }
    }
    ensureGraph(char){
        if(!this.graphs[char]){
            this.graphs[char] = new PointSetGraph();
            this.graphs[char].base = this.base;
            this.graphs[char].char = char;
            if(!this.dontAppendCopiesInAdvance){
                this.appendCopies(char);
            }
        }
    }
    filterAll(friendsNeeded){
        for(var char in this.graphs){
            var graph = this.graphs[char];
            if(this.dontAppendCopiesInAdvance){
                this.appendCopies(char);
            }
            graph.filterWhilePossible(friendsNeeded);
            if(graph.isTrivial()){
                delete this.graphs[char];
            }
        }

        // The copies are on their place anyway
        this.dontAppendCopiesInAdvance = false;

        for(let i = 0; i < this.drafts.length; i++){
            let d = this.drafts[i];
            d.filter(friendsNeeded - d.clique.length);
            if (
                (d.clique.length + d.p.length < friendsNeeded)
            ||
                (this.noFacher && d.isFacherNaive())
            ){
                removeFromArray(this.drafts, i);
                i--;
                continue;
            }
            d.enlargeClique();
            if(!d.p.length){
                this.ready.push(new PointSet(
                    d.char,
                    d.base,
                    d.clique
                ));
                removeFromArray(this.drafts, i);
                i--;
                continue;
            }
            if(this.forkDrafts){
                this.drafts.push(this.drafts[i].fork());
                i--;
            }
        }
        if(this.noFacher){
            for(let i = 0; i < this.ready.length; i++){
                if(this.ready[i].isFacher()){
                    removeFromArray(this.ready, i);
                    i--;
                }
            }
        }
    }
    readFromFileSync(filename){
        var parsed = JSON.parse(fs.readFileSync(filename,'utf8'));
        Object.assign(this, parsed);
        for(let char in this.graphs){
            this.graphs[char] = Object.assign(new PointSetGraph(), this.graphs[char]);
        }
        for(let  num in this.drafts){
            this.drafts[ num] = Object.assign(new PointSetDraft(), this.drafts[ num]);
        }
        for(let  num in this.ready ){
            this.ready [ num] = Object.assign(new PointSet     (), this.ready [ num]);
        }
    }
    separateOneDraft(char){
        let g = this.graphs[char];
        if(g.quad.length){
            let point = g.quad[g.quad.length - 1];
            g.quad.length--;
            let friends = g.getFriends(point);
            let d = new PointSetDraft(
                g.char,
                g.base,
                friends,
                [
                    {x:+((this.base/2)**2), y:0},
                    {x:-((this.base/2)**2), y:0},
                    point,
                ]
            );
            this.drafts.push(d);
        }
    }
}
