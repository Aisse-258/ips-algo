
var child_process = require('child_process');
module.exports = function drawGnuplot(filename){
	child_process.execSync(
		'gnuplot',
		{
			input: [
				'set nokey',
				'set size ratio -1',
				'set term png',
				'set output "' + filename + '.png"',
				'plot "' + filename + '.txt" ps 2 pt 7',
				'exit',
			''].join('\n'),
		}
	)
}
