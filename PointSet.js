'use strict';

const isFullSquare = require('./isFullSquare');
const gcd = require('compute-gcd');
const removeFromArray = require('./removeFromArray');

class PointSet {
    constructor(char, base, points) {
        this.char = char ||  1; // Характеристика
        this.base = base ||  1; // Удвоенная длина основания
        this.p = points  || []; // Собственно точки
    }
    isDistanceIntegral(p1,p2) {
        var s = (p1.x-p2.x)**2 + this.char*(p1.y-p2.y)**2;
        if(!isFullSquare(s)){
            return false;
        }
        if(s%(this.base**2)){
            return false;
        }
        if(!isFullSquare(s/(this.base**2))){
            return false;
        }
        return true;
    }
    deepClone(){
        let ps = new PointSet(this.char,this.base,this.p.slice());
        for(let i = 0; i < this.p.length; i++){
            ps.p[i] = {x:this.p[i].x, y:this.p[i].y};
        }
        return ps;
    }

    countOutOfLineThrough(h,k){
        const x3 = this.p[k].x;
        const y3 = this.p[k].y;
        const A = x3-this.p[h].x;
        const B = y3-this.p[h].y;
        let out = 0;

        for(let i = 0; i < this.p.length; i++){
            if(A*(y3-this.p[i].y) !== B*(x3-this.p[i].x)){
                out++;
            }
        }

        return out;
    }

    isFacher(){
        return (
            this.countOutOfLineThrough(this.p.length - 1, this.p.length - 2) === 1
        ||
            this.countOutOfLineThrough(                0,                 1) === 1        
        )
    }

    shift(x,y){
        for(let p of this.p){
            p.x += x;
            p.y += y;
        }
    }
    reduce(){
        var g = this.base;
        for(let p of this.p){
            g = gcd(g, p.x, p.y);
            if(g == 1){
                return;
            }
        }
        this.base /= g;
        for(let p of this.p){
            p.x /= g;
            p.y /= g;
        }        
    }
    stretch(m){
        this.base *= m;
        for(let i in this.p){
            this.p[i]={
                x: this.p[i].x * m,
                y: this.p[i].y * m,
            }
        }                
    }

    convertToBigInts(){
        this.char = BigInt(this.char);
        this.base = BigInt(this.base);
        for(let i in this.p){
            this.p[i]={
                x: BigInt(this.p[i].x),
                y: BigInt(this.p[i].y),
            }
        }                
    }

    guessCircleCenter(p1, p2, p3){
        this.convertToBigInts();
        var a = BigInt(p2.x - p1.x);
        var b = BigInt(p2.y - p1.y);
        var c = BigInt(p3.x - p1.x);
        var d = BigInt(p3.y - p1.y);

        //console.log(a, b, c, d);
        //console.log(p1,p2,p3);

        var e = a*(p1.x + p2.x) + b*(p1.y + p2.y) * this.char;
        var f = c*(p1.x + p3.x) + d*(p1.y + p3.y) * this.char;
        var g = 2n*(a*(p3.y - p2.y) - b*(p3.x - p2.x)) * this.char;// * sqrt (this.char)

        //если точки на одной прямой или совпадают
        if (g === 0n){
            return null;
        }
        //console.log(g, e, f);
        this.stretch(g);
        var center = {
            x: (d*e - b*f),// * this.char, // /g
            y: (a*f - c*e),// * this.char, // /g
        };
        //console.log('Center:', center);
        //console.log('Base:', this.base);
        return center;
    }

    qasidistance(p1,p2){
        return (p1.x-p2.x)**2n + this.char*(p1.y-p2.y)**2n;
    }

    isCircular(){
        this.convertToBigInts();
        let center = this.guessCircleCenter(this.p[0], this.p[1], this.p[2]);
        if (!center){
            return false;
        }
        var radius = this.qasidistance(center, this.p[0]);
        for (let i = 3; i < this.p.length; i++){
            if(this.qasidistance(center, this.p[i]) != radius){
                return false;
            }
        }
        return true;
    }

    isCrossNaive(){
        let xs = {};
        for(let p of this.p){
            if(p.y != 0){
                xs[p.x] = 1;
            }
        }
        //console.log(this.p);
       // console.log(Object.keys(xs));
        return Object.keys(xs).length <= 1;
    }

    isCross(){
        if(this.isCrossNaive()){
            return true;
        }
        //console.log('Start check...');
        let ps = this.deepClone();
        ps.convertToBigInts();
        //console.log(ps.p);
        ps.shift(-ps.p[0].x, -ps.p[0].y);
        //console.log(ps.p);
        ps.rotateBig(1);
        //console.log(ps.p);
        if(ps.isCrossNaive()){
            return true;
        }
        ps = this.deepClone();
        ps.convertToBigInts();
        ps.shift(-ps.p[1].x, -ps.p[1].y);
        ps.rotateBig(2);
        //console.log(ps.p);
        if(ps.isCrossNaive()){
            return true;
        }
        ps = this.deepClone();
        ps.convertToBigInts();
        ps.shift(-ps.p[0].x, -ps.p[0].y);
        ps.rotateBig(2);
        //console.log(ps.p);
        if(ps.isCrossNaive()){
            return true;
        }
        return false;
    }

    rotate(index){
        let a = this.p[index].x;
        let b = this.p[index].y;
        let m = this.base;
        let q = this.char;
        if(!isFullSquare(a*a+b*b*q)){
            return;
        }

        this.base = m*Math.sqrt(a*a+b*b*q);

        for(let p of this.p){
            let x = p.x, y = p.y;
            p.x = a*x + b*y*q;
            p.y = -b*x + a*y;
        }
    }

    rotateBig(index){
        let a = this.p[index].x;
        let b = this.p[index].y;
        let m = this.base;
        let q = this.char;
        
        let c = a*a+b*b*q;

        this.base = m*c;

        for(let p of this.p){
            let x = p.x, y = p.y;
            p.x = (a*x + b*y*q) * c;
            p.y = (-b*x + a*y) * c;
        }
    }


    prepareDataForGnuplot(){
        var rez = '';
        for(var i=0; i<this.p.length; i++){
            rez += this.p[i].x/this.base + ' ' + this.p[i].y*Math.sqrt(this.char)/this.base + '\n';
        }
        return rez;
    }

    prepareDataForTextExport(){
        var rez = [this.p.length, this.char, this.base].join(' ') + '\n';
        for(var i=0; i<this.p.length; i++){
            rez += this.p[i].x + ' ' + this.p[i].y + '\n';
        }
        return rez;
    }

    applyBronKerbosch(start, targetPow, callbackSuccess){
        if(this.p.length < targetPow){
            // There are too few points
            return false;
        }
        if(this.p.length < start){
            // The end is reached, there are enough points
            // (otherwise the return above should be triggered)
            callbackSuccess(this);
            return true;
        }
        const p1 = this.p.slice(0,start);
        // I don't know, are there more "good" or "bad" points
        // Let's suppose that "good" ones prevail
        for(let i = start + 1; i < this.p.length; i++){
            if(!this.isDistanceIntegral(this.p[start],this.p[i])){
                p1.push(this.p[i]);
                removeFromArray(this.p,i);
                i--;
            }
        }
        // this.p contains now only the friends of this.p[start] (and all the previous)
        // p1 contains only the points which are NOT friends of this.p[start],
        // but ARE friends of this.p[start-1] and all the previous

        const suc1 = this.applyBronKerbosch(start+1, targetPow, callbackSuccess);
        let suc2 = false;
        if(p1.length){
            const p2 = this.p;
            this.p = p1;
            suc2 = this.applyBronKerbosch(start, targetPow, callbackSuccess);
            this.p = p2;
        }
        return suc1 || suc2;
    }
}

module.exports = PointSet;