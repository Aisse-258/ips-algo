'use strict';

const PointSetGraphFactory = require('./PointSetGraphFactory');
const drawGnuplot = require('./drawGnuplot');
const ls = require('ls');
const fs = require('fs');
const crypto = require('crypto');

for(var file of ls(process.argv[2] || 'aux/8_*_sep1.json')){
        /*if(fs.existsSync(file.full.replace('_nofacher1.json','_nofacher2.json'))){
            console.log('File exists: ' + file.full.replace('_nofacher1.json','_nofacher2.json'));
            continue;
        }*/
    var count = 1*file.name.split('_')[0];
    console.log(file, count);
    var psf = new PointSetGraphFactory();
    psf.readFromFileSync(file.full);

    try{
        for(let result of psf.ready){
            let clone = result.deepClone();
            clone.reduce();
            if(clone.isCircular()){
                console.log('Circular!');
                continue;
            }
            clone = result.deepClone();
            if(clone.isCross()){
                console.log('Cross!');
                continue;
            }
            let hash = crypto.createHash('md5').update(JSON.stringify(result)).digest("hex");
            let text = result.prepareDataForGnuplot();
            let filename = 'found/'+[
                result.p.length,
                result.base/2,
                result.char,
                hash,
            ].join('_');
            fs.writeFileSync(filename+'.txt', text);
            drawGnuplot(filename);
            fs.writeFileSync(filename+'.ips.txt', result.prepareDataForTextExport());
        }


    }catch(e){
        console.error(e);
        console.log(file);
        return;
    }
}