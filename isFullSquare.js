'use strict';

function isFullSquare(x){
    return x === Math.floor(Math.sqrt(x))**2;
}

module.exports = isFullSquare;